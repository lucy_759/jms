package com.jamp.jms.service;

import com.jamp.jms.model.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class Sender {

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendQueue(Email email) {
        System.out.println("sending with convertAndSend() to queue <" + email + ">");
        jmsTemplate.convertAndSend("mailbox", email);
    }

}
