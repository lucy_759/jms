package com.jamp.jms.service;

import com.jamp.jms.model.Email;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class Receiver {

    @JmsListener(destination = "mailbox", containerFactory = "containerFactory")
    public void recieveMessage(Email email) {
        System.out.println("Recieved: " + email);
    }

}
